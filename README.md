# Pentachoron plugin template

This is a template for creating new plugins for pentachoron. 

# TODOs

- [ ] Implement the plugin from this template
  
  - [ ] Make a copy of this stuff here
  - [ ] Change your data in pyproject.toml
  - [ ] Change names of
    - [ ] tests
    - [ ] folder name of pentac/plugins/pirate_name_translator
  - [ ] Implement the plugin.py module

- [ ] Install the plugin to your environment

- [ ] Configure the plugin to be loaded (in configuration.yaml or add it to the PentacApp call, if you do this)
  
  ```python
  from pentac.application import PentacApp
  from pentac.configuration import Plugin
  
    def main():
      plugins = [Plugin(module="pentac.plugins.pirate_name_translator.plugin")]
      app = PentacApp(additional_plugins=plugins)
      app.execute()
  
  if __name__ == '__main__':
      main()
  ```

- [ ] You can debug what pentachoron main app detects and how the transformation goes by setting
  
  ```python
  from pentac.application import DebugFlags
  DebugFlags.print_text_translation = True
  ```

- [ ] You can debug the performance aspect by printing the detection profiling data
  
  ```python
  from pentac.application import DebugFlags
  DebugFlags.print_detection_profiling = True
  ```
