from pentac.plugin import PluginManager, PluginInterface, PluginConfiguration, PluginIdentification


@PluginManager.plugin
class PirateNameTranslator(PluginInterface):

    def __init__(self):
        self.identification = PluginIdentification(name="PirateNameTranslator", version="0.1.0")
        self.configuration = PluginConfiguration(match_patterns=["Toothrot[a-zA-Z]*",
                                                                 "Blackeye[a-zA-Z]*",
                                                                 "Barnacle[a-zA-Z]*"],
                                                 match_only_nearest_to_mouse=True)

    def get_identification(self) -> PluginIdentification:
        return self.identification

    def set_plugin_configuration(self, configuration: str = "", config_file: str = None):
        pass

    def get_configuration(self) -> PluginConfiguration:
        return self.configuration

    def get_translation(self, text, match_pattern, match_pattern_index) -> str:
        pirate_name = text
        official_world_name = pirate_name.replace("Toothrot", "Herr ")
        official_world_name = official_world_name.replace("Blackeye", "Herr ")
        official_world_name = official_world_name.replace("Barnacle", "Herr ")
        return f"{pirate_name}: {official_world_name}"
